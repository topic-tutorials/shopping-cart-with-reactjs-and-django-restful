# Deploy Shopping Cart with Docker container

## Tools

* Docker
* Docker Compose

## Download source code

> Use "git clone".

> Click download link of this page.


## Deployment of Shopping Cart Backend 

> Open first terminal.

> cd shopping-cart-with-docker-reactjs-and-django-restful 

> docker-compose up

## Check whether your backend API service is running

> http://localhost:8000/api

## Check whether your can see backend API swagger document

> http://localhost:8000

## Check whether you can use your Shopping Cart.

> http://localhost:3000

## Know multiple methods to update Shopping Cart data

* ReactJS UI of Shopping Cart

* Swagger API

* CURL

* Django Admin

* Postman



